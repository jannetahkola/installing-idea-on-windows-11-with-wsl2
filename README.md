# IntelliJ IDEA on Windows 11 with WSL2 (Ubuntu)

- Windows 11 version 21H2
- Ubuntu & Linux kernel version 5.10.16
- IDEA Ultimate version 2022.1.3

## WSL setup
1. Enable virtualization in your BIOS:
    - SVM on AMD/Gigabyte
2. Enable WSL in an administrator instance of PowerShell:
    - `wsl --install`
    - Reboot, let it finish and create a Linux user
3. Install Java and Maven in Linux
    - Java: `sudo apt install openjdk-17-jdk openjdk-17-jre`
    - Maven:
        ```bash
        # Download binaries
        wget https://dlcdn.apache.org/maven/maven-3/3.8.6/binaries/apache-maven-3.8.6-bin.tar.gz -P /tmp
        # Extract under /opt
        sudo tar xf /tmp/apache-maven-*.tar.gz -C /opt
        # Move contents under "maven"
        sudo mv /opt/apache-maven-3.8.6 /opt/maven
        # Own the file if not already
        sudo chown -R <user>: /opt/maven
        # You could also create a symlink instead of mv, but its permissions cannot be changed so it 
        # probably doesn't work
        # sudo ln -s /opt/apache-maven-3.8.6 /opt/maven
        ```
4. Update path
    - Open `.profile` with `nano .profile`
    - Add these at the end
    ```bash
    export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64
    export M2_HOME=/opt/maven
    export MAVEN_HOME=/opt/maven
    export PATH=${M2_HOME}/bin:${PATH}
    ```
5. Verify
    ```bash
    java -version
    mvn -version
    ```

## IDEA setup in Windows
1. Install IDEA
2. Use WSL terminal
    - Settings -> Tools -> Terminal -> Shell path: `wsl.exe --distribution Ubuntu`
3. Use WSL Maven
    - Settings -> Build, Execution, Deployment -> Build Tools -> Maven -> Maven home path: `\\wsl$\Ubuntu\opt\apache-maven-3.8.6`
    - Make sure the `user settings file` and `local repository` paths match as well
4. Set default project directory inside WSL
    - Settings -> Appearance & Behavior -> System Settings -> Project -> Default project directory: `\\wsl$\Ubuntu\home\<user>\<path>`
5. Set default project directory as a trusted location
    - Settings -> Build, Execution, Deployment -> Trusted Locations -> Add new (+)
6. Create a default run configuration that uses WSL
    - Settings -> Build, Execution, Deployment -> Run targets -> Add new (+) -> WSL -> Select WSL as `Project default target`

## Potential issues

### IDEA stuck on indexing JDK

Add your project folder as an exception for Windows Defender:

`Windows Security` -> `Virus & threat protection` -> `Manage settings` -> `Add or remove exclusions`

### IDEA says "WSL configuration issue" and "Ubuntu does not have configured Maven" during Maven refresh

Make sure both the Maven binary and IDEA project folders are owned by the correct 
Linux user you have set up, not root. If they are owned by root, use `chown`:

```bash
sudo chown -R <user>: <folder>
```

### IDEA fails to create project folder

Change the ownership of the parent folder where you are trying create the project 
in. E.g. `/dev/myproject` -> `sudo chown -R <user>: dev`

### `java.nio.file.AccessDeniedException` during build

See above instructions on how to change the ownership of IDEA's `.cache` folder under 
the user's home directory.

### Autocomplete on external libraries not working and throwing an exception

Seems to happen when trying to autocomplete methods on classes from external libraries. The "fix" seems to be to remove 
`.cache`, `.m2` and other possible IDEA/Maven cache related things from WSL and reinstall IDEA. Possibly also to restart 
Ubuntu before reinstalling.

Exception stacktrace:
```
SEVERE - #c.i.c.c.CompletionProgressIndicator - java.security.PrivilegedActionException: java.nio.file.FileSystemException: \\wsl$\Ubuntu\home\janne\.m2\repository\org\slf4j\slf4j-api\1.7.36\slf4j-api-1.7.36.jar: Incorrect function
java.io.IOException: java.security.PrivilegedActionException: java.nio.file.FileSystemException: \\wsl$\Ubuntu\home\janne\.m2\repository\org\slf4j\slf4j-api\1.7.36\slf4j-api-1.7.36.jar: Incorrect function
	at jdk.zipfs/jdk.nio.zipfs.ZipFileSystem.initOwner(ZipFileSystem.java:262)
	at jdk.zipfs/jdk.nio.zipfs.ZipFileSystem.<init>(ZipFileSystem.java:154)
	at jdk.zipfs/jdk.nio.zipfs.ZipFileSystemProvider.getZipFileSystem(ZipFileSystemProvider.java:125)
	at jdk.zipfs/jdk.nio.zipfs.ZipFileSystemProvider.newFileSystem(ZipFileSystemProvider.java:120)
	at java.base/java.nio.file.FileSystems.newFileSystem(FileSystems.java:528)
	at java.base/java.nio.file.FileSystems.newFileSystem(FileSystems.java:400)
	at com.intellij.recommenders.java.resolver.LibraryOrderEntryPath$Companion.computeDelegatePath(ByOrderEntryResolver.kt:106)
	at com.intellij.recommenders.java.resolver.LibraryOrderEntryPath.<init>(ByOrderEntryResolver.kt:99)
	at com.intellij.recommenders.java.resolver.ModuleRevisionIdService.toModulePath(ModuleRevisionIdService.kt:59)
	at com.intellij.recommenders.java.resolver.ModuleRevisionIdService.resolveModuleRevisionId(ModuleRevisionIdService.kt:47)
	at com.intellij.recommenders.java.resolver.ModuleRevisionIdService.resolveModuleRevisionId(ModuleRevisionIdService.kt:43)
	at com.intellij.recommenders.java.util.PsiUtils.toResolvedType(PsiUtils.kt:129)
	at com.intellij.recommenders.java.engines.InstancesMavenRecommender.recommendations(InstancesMavenRecommender.kt:28)
	at com.intellij.recommenders.java.engines.InstancesContextFeatureProvider.calculateFeatures(InstancesFeatureProviders.kt:149)
	at com.intellij.completion.ml.sorting.ContextFeaturesContributor.calculateContextFactors(ContextFeaturesContributor.kt:46)
	at com.intellij.completion.ml.sorting.ContextFeaturesContributor.fillCompletionVariants(ContextFeaturesContributor.kt:31)
	at com.intellij.codeInsight.completion.CompletionService.getVariantsFromContributors(CompletionService.java:76)
	at com.intellij.codeInsight.completion.CompletionService.getVariantsFromContributors(CompletionService.java:59)
	at com.intellij.codeInsight.completion.CompletionService.performCompletion(CompletionService.java:132)
	at com.intellij.codeInsight.completion.BaseCompletionService.performCompletion(BaseCompletionService.java:43)
	at com.intellij.codeInsight.completion.CompletionProgressIndicator.lambda$calculateItems$11(CompletionProgressIndicator.java:870)
	at com.intellij.util.indexing.FileBasedIndex.lambda$ignoreDumbMode$0(FileBasedIndex.java:202)
	at com.intellij.openapi.util.RecursionManager$1.computePreventingRecursion(RecursionManager.java:114)
	at com.intellij.util.indexing.FileBasedIndexEx.ignoreDumbMode(FileBasedIndexEx.java:663)
	at com.intellij.util.indexing.FileBasedIndex.ignoreDumbMode(FileBasedIndex.java:201)
	at com.intellij.util.indexing.DumbModeAccessType.ignoreDumbMode(DumbModeAccessType.java:43)
	at com.intellij.codeInsight.completion.CompletionProgressIndicator.calculateItems(CompletionProgressIndicator.java:866)
	at com.intellij.codeInsight.completion.CompletionProgressIndicator.runContributors(CompletionProgressIndicator.java:854)
	at com.intellij.codeInsight.completion.CodeCompletionHandlerBase.lambda$startContributorThread$6(CodeCompletionHandlerBase.java:352)
	at com.intellij.codeInsight.completion.AsyncCompletion.lambda$tryReadOrCancel$5(CompletionThreading.java:172)
	at com.intellij.openapi.application.impl.ApplicationImpl.tryRunReadAction(ApplicationImpl.java:1154)
	at com.intellij.codeInsight.completion.AsyncCompletion.tryReadOrCancel(CompletionThreading.java:170)
	at com.intellij.codeInsight.completion.CodeCompletionHandlerBase.lambda$startContributorThread$7(CodeCompletionHandlerBase.java:344)
	at com.intellij.codeInsight.completion.AsyncCompletion.lambda$startThread$0(CompletionThreading.java:95)
	at com.intellij.openapi.progress.impl.CoreProgressManager.lambda$runProcess$2(CoreProgressManager.java:188)
	at com.intellij.openapi.progress.impl.CoreProgressManager.lambda$executeProcessUnderProgress$12(CoreProgressManager.java:608)
	at com.intellij.openapi.progress.impl.CoreProgressManager.registerIndicatorAndRun(CoreProgressManager.java:683)
	at com.intellij.openapi.progress.impl.CoreProgressManager.computeUnderProgress(CoreProgressManager.java:639)
	at com.intellij.openapi.progress.impl.CoreProgressManager.executeProcessUnderProgress(CoreProgressManager.java:607)
	at com.intellij.openapi.progress.impl.ProgressManagerImpl.executeProcessUnderProgress(ProgressManagerImpl.java:60)
	at com.intellij.openapi.progress.impl.CoreProgressManager.runProcess(CoreProgressManager.java:175)
	at com.intellij.codeInsight.completion.AsyncCompletion.lambda$startThread$1(CompletionThreading.java:91)
	at com.intellij.openapi.application.impl.ApplicationImpl$1.run(ApplicationImpl.java:297)
	at java.base/java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:539)
	at java.base/java.util.concurrent.FutureTask.run(FutureTask.java:264)
	at java.base/java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1136)
	at java.base/java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:635)
	at java.base/java.util.concurrent.Executors$PrivilegedThreadFactory$1$1.run(Executors.java:702)
	at java.base/java.util.concurrent.Executors$PrivilegedThreadFactory$1$1.run(Executors.java:699)
	at java.base/java.security.AccessController.doPrivileged(AccessController.java:399)
	at java.base/java.util.concurrent.Executors$PrivilegedThreadFactory$1.run(Executors.java:699)
	at java.base/java.lang.Thread.run(Thread.java:833)
Caused by: java.security.PrivilegedActionException: java.nio.file.FileSystemException: \\wsl$\Ubuntu\home\janne\.m2\repository\org\slf4j\slf4j-api\1.7.36\slf4j-api-1.7.36.jar: Incorrect function
	at java.base/java.security.AccessController.doPrivileged(AccessController.java:573)
	at jdk.zipfs/jdk.nio.zipfs.ZipFileSystem.initOwner(ZipFileSystem.java:253)
	... 51 more
Caused by: java.nio.file.FileSystemException: \\wsl$\Ubuntu\home\janne\.m2\repository\org\slf4j\slf4j-api\1.7.36\slf4j-api-1.7.36.jar: Incorrect function
	at java.base/sun.nio.fs.WindowsException.translateToIOException(WindowsException.java:92)
	at java.base/sun.nio.fs.WindowsException.rethrowAsIOException(WindowsException.java:96)
	at java.base/sun.nio.fs.WindowsAclFileAttributeView.getFileSecurity(WindowsAclFileAttributeView.java:89)
	at java.base/sun.nio.fs.WindowsAclFileAttributeView.getOwner(WindowsAclFileAttributeView.java:122)
	at java.base/sun.nio.fs.FileOwnerAttributeViewImpl.getOwner(FileOwnerAttributeViewImpl.java:91)
	at java.base/java.nio.file.Files.getOwner(Files.java:2202)
	at jdk.zipfs/jdk.nio.zipfs.ZipFileSystem.lambda$initOwner$2(ZipFileSystem.java:252)
	at java.base/java.security.AccessController.doPrivileged(AccessController.java:569)
	... 52 more
```